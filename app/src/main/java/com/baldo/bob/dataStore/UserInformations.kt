package com.baldo.bob.dataStore

data class UserInformations(
    val userName: String,
    val lastPeriodDate: Long,
    val lastOvulationDate: Long?,
    val numberOfWeekForCalculationTerm: Int
)

data class AppSettings(
    val themeSetting: String
)

data class UserLanguage(
    val userLanguage: Int
)