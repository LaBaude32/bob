package com.baldo.bob.dataStore

import android.util.Log
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException

class UserInformationsRepository(
    private val dataStore: DataStore<Preferences>,
    private val defaultLanguage: Int = 1
) {
    private companion object {
        val USER_NAME = stringPreferencesKey("user_name")
        val LAST_PERIOD_DATE = longPreferencesKey("last_period_date")
        val LAST_OVULATION_DATE = longPreferencesKey("last_ovulation_date")
        val NUMBER_OF_WEEKS_FOR_CALCULATION_TERM =
            intPreferencesKey("number_of_weeks_for_calculation_term")
        val THEME_SETTING = stringPreferencesKey("theme_setting")
        val USER_LANGUAGE = intPreferencesKey("user_language")
        const val TAG = "UserPreferencesRepo"
    }

    val userPreferencesFlow: Flow<UserInformations> = dataStore.data.catch {
        if (it is IOException) {
            Log.e(TAG, "Error reading preferences.", it)
            emit(emptyPreferences())
        } else {
            throw it
        }
    }.map { preferences ->
        val userName = preferences[USER_NAME] ?: ""
        val lastPeriodDate = preferences[LAST_PERIOD_DATE] ?: 0
        val lastOvulationDate = preferences[LAST_OVULATION_DATE]
        val numberOfWeeksForCalculationTerm =
            preferences[NUMBER_OF_WEEKS_FOR_CALCULATION_TERM] ?: 38
        UserInformations(
            userName,
            lastPeriodDate,
            lastOvulationDate,
            numberOfWeeksForCalculationTerm
        )
    }

    val appSettingsPreferencesFlow: Flow<AppSettings> = dataStore.data.catch {
        if (it is IOException) {
            Log.e(TAG, "Error reading preferences.", it)
            emit(emptyPreferences())
        } else {
            throw it
        }
    }.map { preferences ->
        val themeSetting = preferences[THEME_SETTING] ?: "system"

        AppSettings(themeSetting)
    }

    val userLanguagePreferencesFlow: Flow<UserLanguage> = dataStore.data.catch {
        if (it is IOException) {
            Log.e(TAG, "Error reading preferences.", it)
            emit(emptyPreferences())
        } else {
            throw it
        }
    }.map { preferences ->
        val userLanguage = preferences[USER_LANGUAGE] ?: defaultLanguage
        UserLanguage(userLanguage)
    }

    suspend fun saveUserInformations(userInformations: UserInformations) {
        dataStore.edit { preferences ->
            preferences[USER_NAME] = userInformations.userName
            preferences[LAST_PERIOD_DATE] = userInformations.lastPeriodDate
            preferences[LAST_OVULATION_DATE] = userInformations.lastOvulationDate ?: 0
            preferences[NUMBER_OF_WEEKS_FOR_CALCULATION_TERM
            ] = userInformations.numberOfWeekForCalculationTerm
        }
    }

    suspend fun saveAppSettings(appSettings: AppSettings) {
        dataStore.edit { preferences ->
            preferences[THEME_SETTING] = appSettings.themeSetting
        }
    }

    suspend fun setUserLanguage(userLanguage: Int) {
        dataStore.edit { preferences ->
            preferences[USER_LANGUAGE] = userLanguage
        }
    }

    fun getDetails() = dataStore.data.map {
        UserInformations(
            userName = it[USER_NAME] ?: "",
            lastPeriodDate = it[LAST_PERIOD_DATE] ?: 0,
            lastOvulationDate = it[LAST_OVULATION_DATE],
            numberOfWeekForCalculationTerm = it[NUMBER_OF_WEEKS_FOR_CALCULATION_TERM] ?: 38
        )
    }

    fun getAppSettings() = dataStore.data.map {
        AppSettings(
            themeSetting = it[THEME_SETTING] ?: "system",
        )
    }

    fun getUserLanguage() = dataStore.data.map {
        UserLanguage(
            userLanguage = it[USER_LANGUAGE] ?: defaultLanguage
        )
    }
}