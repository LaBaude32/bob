package com.baldo.bob.ui.compose

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.BugReport
import androidx.compose.material.icons.filled.Code
import androidx.compose.material.icons.filled.Language
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.baldo.bob.BuildConfig
import com.baldo.bob.R

@Preview(showBackground = true)
@Composable
fun AboutScreen() {
    Column(
        Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val uriHandler = LocalUriHandler.current
        Text(stringResource(id = R.string.about), fontWeight = FontWeight.Black)
        Spacer(modifier = Modifier.height(32.dp))
        Text(stringResource(R.string.application_version, BuildConfig.VERSION_NAME))
        Spacer(modifier = Modifier.height(32.dp))
        Button(onClick = { uriHandler.openUri("https://poeditor.com/join/project/c4b0FCs3UU") }) {
            Icon(
                Icons.Default.Language,
                contentDescription = null,
                modifier = Modifier.padding(end = 8.dp)
            )
            Text(
                stringResource(R.string.translate_app)
            )
        }
        Button(onClick = { uriHandler.openUri("https://gitlab.com/LaBaude32/bob/-/issues") }) {
            Icon(
                Icons.Default.BugReport,
                contentDescription = null,
                modifier = Modifier.padding(end = 8.dp)
            )
            Text(
                stringResource(R.string.report_bug)
            )
        }
//        Button() { Text("Traduire l'application") }
        Button(onClick = { uriHandler.openUri("https://gitlab.com/LaBaude32/bob") }) {
            Icon(
                Icons.Default.Code,
                contentDescription = null,
                modifier = Modifier.padding(end = 8.dp)
            )
            Text(
                stringResource(R.string.see_code)
            )
        }
    }
}