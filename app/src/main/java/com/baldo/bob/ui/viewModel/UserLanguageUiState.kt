package com.baldo.bob.ui.viewModel

data class UserLanguageUiState(
val userLanguage: Int = 1
)
