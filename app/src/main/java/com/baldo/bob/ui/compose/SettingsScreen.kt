package com.baldo.bob.ui.compose

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.selection.selectableGroup
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Language
import androidx.compose.material.icons.filled.Translate
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.baldo.bob.R
import com.baldo.bob.dataStore.AppSettings
import com.baldo.bob.ui.viewModel.AppUiState
import com.baldo.bob.ui.viewModel.BobViewModel
import com.baldo.bob.ui.viewModel.UserLanguageUiState

@Composable
fun SettingsScreen(
    bobViewModel: BobViewModel,
    appUiState: AppUiState,
    userLanguageUiState: UserLanguageUiState,
) {
    val currentLanguage = userLanguageUiState.userLanguage
    Column(
        Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(stringResource(id = R.string.settings), fontWeight = FontWeight.Black)
        Spacer(modifier = Modifier.height(32.dp))
        Text(text = stringResource(R.string.theme) + " :")
        ThemeSwitcher(bobViewModel, appUiState)
        Spacer(modifier = Modifier.height(32.dp))
        LanguageSelector(currentLanguage) { selected -> bobViewModel.updateUserLanguage(selected) }
    }
}

@Composable
fun ThemeSwitcher(bobViewModel: BobViewModel, appUiState: AppUiState) {
    Column {
        val themeSetting: String = appUiState.themeSetting
        Row(verticalAlignment = Alignment.CenterVertically) {
            Switch(
                checked = themeSetting == "system",
                onCheckedChange = {
                    bobViewModel.updateAppSettings(AppSettings(themeSetting = "system"))
                })
            Text(text = stringResource(R.string.system), Modifier.padding(start = 16.dp))
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Switch(
                checked = themeSetting == "light",
                onCheckedChange = {
                    bobViewModel.updateAppSettings(AppSettings(themeSetting = "light"))
                })
            Text(text = stringResource(R.string.light), Modifier.padding(start = 16.dp))
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Switch(
                checked = themeSetting == "dark",
                onCheckedChange = {
                    bobViewModel.updateAppSettings(AppSettings(themeSetting = "dark"))
                })
            Text(text = stringResource(R.string.dark), Modifier.padding(start = 16.dp))
        }
    }
}

//private val language = listOf("English", "French", "Italian", "D", "E", "F", "G", "H", "I","A", "B", "C", "D", "E", "F", "G", "H", "I")
data class Language(
    val langTag: String,
    val displayName: String
)

val langs = listOf<Language>(
    Language("fr", "Français"),
    Language("en", "English"),
    Language("it", "Italian"),
    Language("de", "Deutsch")
)

@Composable
fun LanguageSelector(selectedPosition: Int, onLanguageSelected: (Int) -> Unit) {
    var showLanguageMenu by remember { mutableStateOf(false) }
    Column {
        Row {
            Button(onClick = { showLanguageMenu = true }) {
                Text(text = stringResource(R.string.language))
                Icon(
                    Icons.Default.Language,
                    contentDescription = null,
                    modifier = Modifier.padding(start = 8.dp)
                )
            }
//            DropdownMenu(
//                expanded = showLanguageMenu,
//                onDismissRequest = { showLanguageMenu = false }
//            ) {
//                items.forEachIndexed { index, s ->
//                    DropdownMenuItem(
//                        onClick = {
//                            selectedIndex = index
//                            showLanguageMenu = false
//                        },
//                        text = { Text(text = s)})
//                }
//            }
        }
    }
    when {
        showLanguageMenu -> {
            LanguageDialog(
                onDismissRequest = { showLanguageMenu = false },
                onConfirmation = { showLanguageMenu = false },
                dialogTitle = stringResource(R.string.choose_language),
                items = langs,
                icon = Icons.Default.Translate,
                selectedPosition = selectedPosition,
                onLanguageSelected = onLanguageSelected
            )
        }
    }
}

@Composable
fun LanguageDialog(
    onDismissRequest: () -> Unit,
    onConfirmation: () -> Unit,
    dialogTitle: String,
    items: List<Language>,
    icon: ImageVector,
    selectedPosition: Int,
    onLanguageSelected: (Int) -> Unit
) {
    val scrollState = rememberScrollState()
    AlertDialog(
        modifier = Modifier.heightIn(0.dp, 700.dp),
        icon = {
            Icon(icon, contentDescription = null)
        },
        title = {
            Text(text = dialogTitle)
        },
        text = {
            Column(
                Modifier
                    .selectableGroup()
                    .verticalScroll(scrollState)
            ) {
                items.forEachIndexed { index, it ->
                    Row(
                        Modifier
                            .fillMaxWidth()
                            .height(56.dp)
                            .selectable(
                                selected = (index == selectedPosition),
                                onClick = { onLanguageSelected(index) },
                                role = Role.RadioButton
                            )
                            .padding(horizontal = 16.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ) {

                        RadioButton(
                            selected = (index == selectedPosition),
                            onClick = null // null recommended for accessibility with screenreaders
                        )
                        Text(
                            text = it.displayName,
                            style = MaterialTheme.typography.bodyLarge,
                            modifier = Modifier.padding(start = 16.dp)
                        )
                    }
                }
            }
        },
        onDismissRequest = {
            onDismissRequest()
        },
        confirmButton = {
            TextButton(
                onClick = {
                    onConfirmation()
                }
            ) {
                Text(stringResource(R.string.close))
            }
        },
    )
}